from src.summary_builder import normalize_summary
from src.summary_builder import Summary
from src.summary_builder import build_summary_object
from src.duration import Duration


def test_normalize_summary_v1():
    raw_data = {
        'Meeting title': 'Practice Modulo 1 - Grupo 4',
        'Attended participants': '1',
        'Start time': '10/10/22, 5:37:48 PM',
        'End time': '10/10/22, 5:38:21 PM',
        'Meeting duration': '33s',
        'Average attendance time': '26s',
        'Meeting Id': 'Practice Modulo 1 - Grupo 4'
    }

    expected_summary = {
        'Title': 'Practice Modulo 1 - Grupo 4',
        'Id': 'Practice Modulo 1 - Grupo 4',
        'Attended participants': '1',
        'Start Time': '10/10/22, 5:37:48 PM',
        'End Time': '10/10/22, 5:38:21 PM',
        'Duration': {
            'hours': 0,
            'minutes': 00,
            'seconds': 33
        }
    }

    assert normalize_summary(raw_data) == expected_summary

   
    

def test_normalize_summary_v2():
    raw_data = {
            'Attended participants': '17', 
            'Meeting title': 'General', 
            'Start time': '10/18/22, 5:49:42 PM', 
            'End time': '10/18/22, 7:53:52 PM', 
            'Meeting Id': '220492dd-c020-49b9-bb1c-64b665217a8d'
    }

    expected_summary = {
        'Title': 'General',
        'Id': '220492dd-c020-49b9-bb1c-64b665217a8d',
        'Attended participants': '17',
        'Start Time': '10/18/22, 5:49:42 PM',
        'End Time': '10/18/22, 7:53:52 PM',
        'Duration': {
            'hours': 2,
            'minutes': 4,
            'seconds': 10
        }
    }

    assert normalize_summary(raw_data) == expected_summary

def test_build_summary_object():
    raw = {
        'Meeting title': 'General',
        'Meeting Id': '220492dd-c020-49b9-bb1c-64b665217a8d',
        'Attended participants': 7,
        'Start time': '10/18/22, 5:49:42 PM',
        'End time': '10/18/22, 7:53:52 PM',
        'Duration': {
            'hours': 2,
            'minutes': 4,
            'seconds': 10
        }
    }

    summary = build_summary_object(raw)
    assert summary is not None
    assert type(summary) is Summary

    assert summary.id == raw['Meeting Id']
    assert summary.title == raw['Meeting title']
    assert summary.attended_participants == raw['Attended participants']
    assert summary.start_time == raw['Start time']
    assert summary.end_time == raw['End time']
    
    assert type(summary.duration) is Duration
    assert summary.duration.hours == raw['Duration']['hours']
    assert summary.duration.minutes == raw['Duration']['minutes']
    assert summary.duration.seconds == raw['Duration']['seconds']


